using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common.CQRS;
using Domain.Users.Commands;
using FluentValidation;
using Microsoft.EntityFrameworkCore;

namespace Domain.Users.CommandValidators
{
    public class RegisterUserValidator : FluentValidator<RegisterUser>
    {
        private readonly IUserRepository _repository;

        public RegisterUserValidator(IUserRepository repository)
        {
            _repository = repository;
            RuleFor(user => user.UserName)
                .NotNull()
                .MustAsync(BeUnique).WithMessage("The username already exist");

            RuleFor(user => user.FullName).NotNull();

            RuleFor(user => user.Password)
                .NotNull()
                .MinimumLength(6);

            RuleFor(user => user.ConfirmPassword)
                .Must((user, s) => s == user.Password)
                .WithMessage("Confirm Password must be the same as Password");
        }


        private async Task<bool> BeUnique(string username, CancellationToken callToken)
        {
            var anyAsync = await _repository.GetAll()
                .Where(user => user.UserName == username)
                .AnyAsync(cancellationToken: callToken);
            return !anyAsync;
        }
    }
}