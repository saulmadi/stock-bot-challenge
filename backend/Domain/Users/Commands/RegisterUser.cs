

using Common.CQRS;

namespace Domain.Users.Commands
{
    public class RegisterUser : ICommand
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string FullName { get; set; }

        public RegisterUser(string userName, string password, string confirmPassword, string fullName)
        {
            UserName = userName;
            Password = password;
            ConfirmPassword = confirmPassword;
            FullName = fullName;
        }

    }
}