using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Entities;
using Domain.Chat.Entities;

namespace Domain.Users.Entities
{
    [Table("User")]
    public class User : IEntity
    {
        protected User()
        {
        }

        public User(string userName, string fullName)
        {
            UserName = userName;
            FullName = fullName;
        }

        [Key] public int Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; }
        public string Password { get; set; }
    }
}