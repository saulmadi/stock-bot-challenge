using System.Threading.Tasks;
using Common.CQRS;
using Domain.Users.Commands;
using Domain.Users.Entities;
using Domain.Users.Services;

namespace Domain.Users.Handlers
{
    public class UserCreator : ICommandHandler<RegisterUser>
    {
        private readonly IUserRepository _repository;
        private readonly IPasswordGenerator _generator;

        public UserCreator(IUserRepository repository, IPasswordGenerator generator)
        {
            _repository = repository;
            _generator = generator;
        }

        public Task Handle(RegisterUser command)
        {
            var user = new User(command.UserName, command.FullName);
            
            user.Password = _generator.HashPassword(user, command.Password);


           return _repository.Create(user);
        }
    }
}