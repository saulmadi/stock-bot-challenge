using Common.Entities;
using Domain.Users.Entities;

namespace Domain.Users
{
    
    public interface IUserRepository : IRepository<User>
    {
        
    }
}