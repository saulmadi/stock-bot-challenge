using System.Threading.Tasks;

namespace Domain.Bot.BotStrategies
{
    public interface ISockApi
    {
        Task<StockModel> GetStock(string stockCode);
    }
}