using System.Threading.Tasks;
using Common;
using Domain.Chat.Entities;

namespace Domain.Bot.BotStrategies
{
    public class Error : AbstractBotStrategy
    {
       
        public override string Command { get; } = "Error";
        public override async Task Reply(Message message)
        {

            await Notify(message, "Command doesn't exist");

        }

      

        public Error(INotificationService service) : base(service)
        {
        }
    }
}