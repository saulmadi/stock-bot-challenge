using System;
using System.Threading.Tasks;
using Common;
using Domain.Chat.Entities;

namespace Domain.Bot.BotStrategies
{
    public class StockCommandStrategy : AbstractBotStrategy
    {
        private readonly ISockApi _api;

        public StockCommandStrategy(INotificationService service, ISockApi api):
            base(service)
        {
            
            _api = api;
        }

        public override string Command { get; } = "/stock";

        public override async Task Reply(Message message)
        {
            var split = message.Content.Split('=');

            if (split.Length < 2)

            {
                var theCommandHasABadFormat = "The command has a bad format";
               await Notify(message, theCommandHasABadFormat);
               return;
               
            }

            var stockCode = split[1];
            var api = await _api.GetStock(stockCode);

            await Notify(message, $"{api.Symbol} quote is {api.Close} per share");

        }
    }
}