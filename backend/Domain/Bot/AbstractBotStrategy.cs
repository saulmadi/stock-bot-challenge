using System;
using System.Threading.Tasks;
using Common;
using Domain.Chat.Entities;

namespace Domain.Bot
{
    public abstract class AbstractBotStrategy : IBotCommandStrategy
    {
        public AbstractBotStrategy(INotificationService service)
        {
            _service = service;
        }
        protected INotificationService _service;
        public abstract string Command { get; }

        public virtual bool CanHandle(Message message)
        {
            return message.Content.ToLower().StartsWith(Command.ToLower());
        }

        public abstract Task Reply(Message message);

        protected async Task Notify(Message message, string content)
        {
            await _service.NotifyMessage(new MesssagePayload
            {
                ChatId = message.ChatRoomId,
                Content = content,
                MessageId = message.Id,
                Owner = "@Bot",
                Date = DateTime.Now
            });
        }
    }
}