using System.Linq;
using System.Threading.Tasks;
using Common.CQRS;
using Domain.Chat.Entities;
using Domain.Chat.Events;
using Domain.Chat.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Domain.Bot.EventHandlers
{
    public class ProcessMessages : IEventHandler<MessageSent>
    {
        private readonly IMessageRepository _repository;
        private readonly IBot _bot;

        public ProcessMessages(IMessageRepository repository, IBot bot)
        {
            _repository = repository;
            _bot = bot;
        }
        public async Task Handle(MessageSent e)
        {
            var message = await _repository.GetAll()
                .Where(message1 => message1.Id ==e.MessageId)
                .Include((d) =>d.Owner)
                .FirstAsync();

            await _bot.ProcessMessage(message);
        }
    }
}