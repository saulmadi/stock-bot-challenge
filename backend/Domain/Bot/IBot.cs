using System.Threading.Tasks;
using Domain.Chat.Entities;

namespace Domain.Bot
{
    public interface IBot
    {
        Task ProcessMessage(Message message);
        Task ProcessMessage(string command, Message message);
    }
}