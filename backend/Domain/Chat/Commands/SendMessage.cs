using Common.CQRS;

namespace Domain.Chat.Commands
{
    public class SendMessage : ICommand
    {
        public int ChatId { get; set; }
        public int UserId { get; set; }
        public string Content { get; set; }
    }
}