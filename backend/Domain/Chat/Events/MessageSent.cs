using Common.CQRS;

namespace Domain.Chat.Events
{
    public class MessageSent : IEvent
    {
        public int MessageId { get; }
        public int ChatId { get; }

        public MessageSent(int messageId, int chatId)
        {
            MessageId = messageId;
            ChatId = chatId;
        }
    }
}