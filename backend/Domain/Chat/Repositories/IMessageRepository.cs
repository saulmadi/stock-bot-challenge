using Common.Entities;
using Domain.Chat.Entities;

namespace Domain.Chat.Repositories
{
    public interface IMessageRepository : IRepository<Message>
    {
        
    }
}