using System.Threading.Tasks;
using Common.CQRS;
using Domain.Chat.Commands;
using Domain.Chat.Entities;
using Domain.Chat.Events;
using Domain.Chat.Repositories;
using Domain.Users;

namespace Domain.Chat.Handlers
{
    public class MesssageSender : ICommandHandler<SendMessage>
    {
        private readonly IChatRoomRepository _repository;
        private readonly IUserRepository _userRepository;
        private readonly IEventDispatcher _dispatcher;

        public MesssageSender(IChatRoomRepository repository, IUserRepository  userRepository, IEventDispatcher dispatcher)
        {
            _repository = repository;
            this._userRepository = userRepository;
            _dispatcher = dispatcher;
        }
        public async Task Handle(SendMessage command)
        {
            var chat =await _repository.GetById(command.ChatId);
            var user = await _userRepository.GetById(command.UserId);
            
            
            var message = new Message(command.Content, user);
            chat.AddMessage(message);

            await _repository.Update(chat);


            await _dispatcher.Dispatch(new MessageSent(message.Id, chat.Id ));

        }
    }
}