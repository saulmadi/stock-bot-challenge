using Common.CQRS;
using Domain.Chat.Commands;
using FluentValidation;
using Microsoft.AspNetCore.Identity;

namespace Domain.Chat.Validators
{
    public class CreateChatRoomValidator : FluentValidator<CreateChatRoom>
    {
        public CreateChatRoomValidator()
        {
            RuleFor(room => room.Name).NotNull();
        }
        
    }
}