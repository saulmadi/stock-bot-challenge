using Common.CQRS;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WebApplication
{
    public class HttpResponseExceptionFilter:  IActionFilter, IOrderedFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (!(context.Exception is NotValidException exception)) return;
            
            context.Result = new ObjectResult( new {exception.Errors})
            {
                StatusCode = 400,
                
            };
            context.ExceptionHandled = true;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            
        }

        public int Order { get; } =  int.MaxValue - 10;
    }
}