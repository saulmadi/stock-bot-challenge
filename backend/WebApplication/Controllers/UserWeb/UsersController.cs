using System.Collections.Generic;
using System.Threading.Tasks;
using Common.CQRS;
using Domain.Users;
using Domain.Users.Commands;
using Domain.Users.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebApplication.Controllers.UserWeb
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ICommandDispatcher _dispatcher;
        private readonly IUserRepository _repository;

        public UsersController(ICommandDispatcher dispatcher, IUserRepository repository)
        {
            _dispatcher = dispatcher;
            _repository = repository;
        }


        [HttpPost]
        public async Task<ActionResult> CreateUser(UserRequest request)
        {
            var registerUser = new RegisterUser(request.UserName, request.Password, request.ConfirmPassword,
                request.FullName);
            
            await _dispatcher.Dispatch(registerUser);

            return Accepted();
        }

        [HttpGet]
        public  Task<List<User>> GetUsers()
        {
            return  _repository.GetAll().ToListAsync();
        }
    }
}