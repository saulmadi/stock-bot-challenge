namespace WebApplication.Controllers.ChatRoomWeb
{
    public class ChatRoomResponse   
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}