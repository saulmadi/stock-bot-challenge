using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Common.CQRS;
using Domain.Chat.Commands;
using Microsoft.AspNetCore.SignalR;

namespace WebApplication.Controllers.ChatRoomWeb
{
    public class ChatWebSocket : Hub<IChatWebSocket>
    {
        private readonly ICommandDispatcher _dispatcher;

        public ChatWebSocket(ICommandDispatcher dispatcher)
        {
            _dispatcher = dispatcher;
        }
        public async Task SendMessageToChat(int chatId, int userId, string message)
        {
            await _dispatcher.Dispatch(new SendMessage(){Content = message, ChatId = chatId,UserId = userId});
        }
    }

    public interface IChatWebSocket
    {

        Task ReceiveMessage(string message);

    }
}