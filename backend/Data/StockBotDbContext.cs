using System.Linq;
using Common.Entities;
using Domain.Chat.Entities;
using Domain.Users.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Data
{
    public class StockBotDbContext : DbContext
    {
        public StockBotDbContext(DbContextOptions<StockBotDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");

            var entitiesTypes = typeof(User).Assembly.GetTypes()
                .Where(type => type.GetInterfaces().Any(type1 => type1 == typeof(IEntity)));

            foreach (var entitiesType in entitiesTypes)
            {
                modelBuilder.Entity(entitiesType);
            }
            
           
            base.OnModelCreating(modelBuilder);
        }
    }
}