using Domain.Chat.Entities;
using Domain.Chat.Repositories;

namespace Data
{
    public class MessageRepository : StockBotRepository<Message>, IMessageRepository
    {
        public MessageRepository(StockBotDbContext dbContext) : base(dbContext)
        {
        }
    }
}