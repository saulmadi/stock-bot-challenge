using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using static DotNetEnv.Env;

namespace Data
{
    public class StockBotDbContextDesignFactory :  IDesignTimeDbContextFactory<StockBotDbContext>
    {
        public StockBotDbContext CreateDbContext(string[] args)
        {
           Load("../WebApplication/.env");
            var optionsBuilder = new DbContextOptionsBuilder<StockBotDbContext>();
            optionsBuilder.UseNpgsql(DotNetEnv.Env.GetString("DB_CONNECTIONSTRING"));

            return new StockBotDbContext(optionsBuilder.Options); 
        }
    }
}