using System.Collections.Generic;
using Autofac;
using Common.CQRS;

namespace Infrastructure
{
    public class AutofacDependencyResolver : IDependencyResolver
    {
        private readonly ILifetimeScope _scope;

        public AutofacDependencyResolver(ILifetimeScope scope)
        {
            _scope = scope;
        }
        public ICommandHandler<TCommand> ResolveCommandHandler<TCommand>(TCommand command) where TCommand : ICommand
        {
            return _scope.Resolve<ICommandHandler<TCommand>>();
        }

        public ICommandValidator<TCommand> ResolveCommandValidator<TCommand>(TCommand command) where TCommand : ICommand
        {
            return _scope.Resolve<ICommandValidator<TCommand>>();
        }

        public IEnumerable<IEventHandler<TEvent>> ResolverEventHandlers<TEvent>(TEvent @event) where TEvent : IEvent
        {
            return _scope.Resolve<IEnumerable<IEventHandler<TEvent>>>();
        }
    }
}