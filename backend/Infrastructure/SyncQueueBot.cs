using System.Threading.Tasks;
using Domain.Bot;
using Domain.Chat.Entities;

namespace Infrastructure
{
     class SyncQueueBot 
    {
       
        public Task SendToQueue(IBotCommandStrategy strategy, Message message)
        {

            return strategy.Reply(message);

        }
    }
}