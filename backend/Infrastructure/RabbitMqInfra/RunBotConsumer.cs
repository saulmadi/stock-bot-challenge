using System.Linq;
using System.Threading.Tasks;
using Common;
using Domain.Bot;
using Domain.Chat.Repositories;
using MassTransit;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.RabbitMqInfra
{
    public class RunBotConsumer : MassTransit.IConsumer<RunBot>
    {
        private readonly IBot _bot;
        private readonly IMessageRepository _repository;

        public RunBotConsumer(IBot bot, IMessageRepository repository)
        {
            _bot = bot;
            _repository = repository;
        }

        public async Task Consume(ConsumeContext<RunBot> context)
        {
            var queueBotCommand = context.Message;

            var message = await _repository.GetAll()
                .Where(message1 => message1.Id == queueBotCommand.MessageId)
                .Include(message1 => message1.Owner)
                .FirstAsync();

            await _bot.ProcessMessage(queueBotCommand.Strategy, message);
        }
    }
}