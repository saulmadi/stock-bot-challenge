using Common.CQRS;

namespace Common.Test
{
    public class FakeCommand : ICommand
    {
        public string Name { get; set; }
    }
}