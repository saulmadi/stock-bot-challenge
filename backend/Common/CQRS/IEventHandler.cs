using System.Threading.Tasks;

namespace Common.CQRS
{
    public interface IEventHandler<TEvent> where TEvent: IEvent
    {
        Task Handle(TEvent e);

    }
}