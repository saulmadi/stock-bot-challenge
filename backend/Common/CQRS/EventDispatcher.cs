using System.Linq;
using System.Threading.Tasks;

namespace Common.CQRS
{
    public  class EventDispatcher : IEventDispatcher
    {
        private readonly IDependencyResolver _dependencyResolver;

        public EventDispatcher(IDependencyResolver dependencyResolver)
        {
            _dependencyResolver = dependencyResolver;
        }
        
        public async Task Dispatch<TEvent>(TEvent e) where TEvent : IEvent
        {
            var handlers =_dependencyResolver.ResolverEventHandlers(e);


            var taks = handlers.Select(handler => handler.Handle(e));

            await Task.WhenAll(taks.ToArray());
        }
    }
}