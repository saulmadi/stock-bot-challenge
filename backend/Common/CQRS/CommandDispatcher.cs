using System.Threading.Tasks;

namespace Common.CQRS
{
    public class CommandDispatcher : ICommandDispatcher

    {
        private IDependencyResolver _dependencyResolver;


        public CommandDispatcher(IDependencyResolver dependencyResolver)
        {
            _dependencyResolver = dependencyResolver;
        }

        public async Task Dispatch<TCommand>(TCommand command) where TCommand : ICommand
        {
            var commandHandler = _dependencyResolver.ResolveCommandHandler(command);

            var validaror = _dependencyResolver.ResolveCommandValidator(command);

            await validaror.ValidateCommand(command);
            await commandHandler.Handle(command);
        }
    }
}