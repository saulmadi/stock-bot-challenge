using System;
using System.Threading.Tasks;

namespace Common
{
    public interface INotificationService
    {
        Task NotifyMessage(MesssagePayload message);
    }

    public class MesssagePayload
    {
        public int ChatId { get; set; }
        public int MessageId { get; set; }
        public string Content { get; set; }
        public string Owner { get; set; }
        public DateTime Date { get; set; }
    }
}